<!-- .slide: data-background-color="#151515" -->
<span class="menu-title" style="display: none">GitMerge</span>

<img data-src="img/Git-merge-2018/git-merge-logo.png" height="500" class="plain">

---
<!-- .slide: data-background-image="img/Git-merge-2018/Git-merge-2018.002.jpeg" -->

Note:
Hello and thank you. As Neha mentioned, My name William Chia, I do product marketing at GitLab, and you can find me on Twitter with the handle @thewilliamchia. So please do take out your phones, and during my talk you can tweet questions and comments using the hash tag #GitMerge. I’ll be sure to reply to and retweet you when I come off stage. 

So, to kick things off I’d like to show the GitLab activity chart for a few of my colleagues. 

---
<!-- .slide: data-background-image="img/Git-merge-2018/Git-merge-2018.003.jpeg" -->

Note:
These are the charts for  Erica, Luke, Jamie and Chad. It shows a summary of their issues, merge requests, push events, and comments. Now, based on their activity, if you had guess what is their job function? what would you say? 
Would you say, “Well, they all look fairly active, they must all be software developers.” Right? 
Or would you say, “I saw the title of your talk, so they are probably not software developers.” 
well, if it's the latter, you’d be right

---
<!-- .slide: data-background-image="img/Git-merge-2018/Git-merge-2018.004.jpeg" -->

Note:
Erica manages the Content Marketing team

---
<!-- .slide: data-background-image="img/Git-merge-2018/Git-merge-2018.005.jpeg" -->

Note:
Luke, is a Graphic Designer 

---
<!-- .slide: data-background-image="img/Git-merge-2018/Git-merge-2018.006.jpeg" -->

Note:
Jamie is our Legal Counsel 

---
<!-- .slide: data-background-image="img/Git-merge-2018/Git-merge-2018.007.jpeg" -->

Note:
And Chad is Head of Sales. 

So these are clearly folks in non-technical roles, but looking at the activity graph you can see they use a lot of GitLab. And it's not just comments, they're using Git itself in a meaningful way. So while you probably won’t find them inspecting tree objects on the command line, you will find them doing things like branching, merging, and committing as part of their everyday workflow. And that’s what this talk is about. 

---
<!-- .slide: data-background-image="img/Git-merge-2018/Git-merge-2018.008.jpeg" -->

Note: 
Empowering non-developers to use Git. In the next 25 minutes I’ll share with you some real-word examples of non-technical folks using Git. Along the way I’ll outline the technology and processes needed that make it happen so that you can put them in place in your org.

We're all here at GitMerge because we love Git. But think about your company, wouldn’t it be amazing if not just your engineering department used Git, but if your entire company did? 

That's what I want to equip you to do.

---
### [gitlab.com/williamchia/git-merge-2018](https://gitlab.com/williamchia/git-merge-2018)

Note:
So, of course to aid you on your journey to evangelize git to non-developers, I have created a repo on GitLab.com. You can go to gitlab.com/williamchia/git-merge-2018
The repo contains a copy of this slide deck, along with links to all the resources I mention in this talk.  

---
<span class="menu-title" style="display: none">Roadmap</span>
<!-- .slide: data-background-image="img/Git-merge-2018/roadmap.001.jpeg" -->

Note:
So here's they key points we're going to hit. 
First, if you're going to use version control, you need something to version. Then you need somewhere to publish, finally, you need some way to stop explosions. So that's a look ahead.  

---
<span class="menu-title" style="display: none">The Stack</span>
<!-- .slide: data-background-image="img/Git-merge-2018/Git-merge-2018.011.jpeg" -->

Note: 
So here’s note about the stack before I jump in. At the lowest level you have Git, and then on top of that you have providers that offer some abstractions. I’ll be sharing some tech and tools you can pair with one of these providers to get almost anyone using Git. 
For this talk, I’ll be showing what this looks like on GitLab, since that’s what I use, but for the most part everything should be provider-agnostic. If I know for a fact something is GitLab-only I’ll try to call that. 

---
<span class="menu-title" style="display: none">Something to version</span>
<!-- .slide: data-background-image="img/Git-merge-2018/roadmap.002.jpeg" -->

Note: 
So step 1 - if you are going to using distributed version control, you need something to version. The obvious thing to use Git for is to version your source code. 

But, if you are not a software developer, what are you going to stick in Git?

---
<!-- .slide: data-background-image="img/Git-merge-2018/Git-merge-2018.015.jpeg" -->

Note:
The answer is markdown

---
<!-- .slide: data-background-image="img/Git-merge-2018/Git-merge-2018.016.jpeg" -->

Note:
And a little bit of YAML as well. 

So, what we'll be doing is creating a system where is becomes really easy for users to create content in markdown instead of putting that content into some other format (like google doc, or a powerpoint) And not only are we going to make that easy, we’re also going to make that beneficial - so that user is happy they using Markdown and see the benefits. 

---
### Markdown Attributes
- Text-based <!-- .element: class="fragment"  -->
- Instantly readable <!-- .element: class="fragment"  -->
- Easy to learn <!-- .element: class="fragment"  -->

Note:
Markdown has these attributes that make it prefect for the task of empowering non-developers to use Git. 

First, it's text-based - this means you can version it just like source code, you the the advantage of diffs, and the ability for multiple people to collaborate on the same thing at the same time. 

Next, it's instantly readable. Even if you don't know what Markdown is, the syntax is so simple you can basically figure out what's happening the very first time you look at it. 

And that of course, makes it very easy to learn. It only takes about 5 minutes for most people to lean enough Markdown to become productive. 

---
### How to evangelize Markdown

Note: 
So, how do we evangelize Markdown? 
Before we can drive Git adoption, we need to evangelize the component technologies that are going to empower the user. Here's how we do that at GitLab. 

https://gitlab.com/gitlab-com/marketing/general/issues/2223

+++
<!-- .slide: data-background-image="img/Git-merge-2018/pricing-page-issue.png" -->

Note: 
We use the GitLab issue tracker for all of our project management whether it’s related to code or not. For example here’s an issue to update the layout for the pricing page on our website. 
(As a side note, We also have a very unique, transparent culture where we make everything public by default if we can. So this is actually a publicly viewable issue. While this type of radical transparency isn't necessarily required to drive Git adoption, I highly encourage it!)

So everyone at GitLab is using issues, 

+++
<!-- .slide: data-background-image="img/Git-merge-2018/Git-merge-2018.019.jpeg" -->

Note:
And in the issue description and comments you can use Markdown to add formatting and structure. 

+++
<!-- .slide: data-background-image="img/Git-merge-2018/Git-merge-2018.020.jpeg" -->

Note:
Of course, we placed an easy-to-find link that tells you Markdown is supported. And when you click on it… 

https://gitlab.com/help/user/markdown

+++
<!-- .slide: data-background-image="img/Git-merge-2018/Git-merge-2018.021.jpeg" -->

Note:
You get taken to a nice reference page that teaches you how to use Markdown. So this all becomes very self-service. Folks join GitLab, they start creating issues, and they pretty much figure out Markdown on their because we make the resources easy-to-find. 

So now you’ve got folks used to using Markdown in issues, but what we really want is them building Markdown files, storing them in Git and leveraging distributed version control. 

---
<span class="menu-title" style="display: none">Somewhere to publish</span>
<!-- .slide: data-background-image="img/Git-merge-2018/roadmap.003.jpeg" -->

Note:
And for that we need a way to publish Markdown files into a styled and consumable format.

So, for this we’re going to use…  

---
### Static Site Generator

Note:
A static site generator. 
Static site generators are different from web CMS tools or single page applications where the content in generated dynamically. A static site generator compiles your website down into static assets - html, CSS, and javascript that you can then serve directly.

Static sites are nice because they are really fast. And they have this awesome property that your users can write a page in markdown, and the static site generator will convert that into and fully styled HTML page. 

---
### Git + Markdown + SSG == CMS

Note: 
So Git, Markdown, and your static Site Generator become your content management system. This is what GitLab uses this across our entire org. 

---
<!-- .slide: data-background-image="img/Git-merge-2018/Git-merge-2018.024.jpeg" -->

Note:
At GitLab we use a markdown-driven static site generator to produce our Marketing website, our blog, our handbook (which contains information that other companies would put into their company wiki, but we put it publicly out on the web for everyone to read) and our docs. 

---
<!-- .slide: data-background-image="img/Git-merge-2018/Git-merge-2018.025.jpeg" -->

Note:
The marketing site, wiki, and blog all run on a static site generator called Middleman. 
 
https://middlemanapp.com/

Our docs site runs on Nanoc 

https://nanoc.ws/

But you don't have to use the same tools as us. 

---
<!-- .slide: data-background-image="img/Git-merge-2018/Git-merge-2018.026.jpeg" -->

Note:
There are Static Site Generators for many languages including Ruby, Node, Go, and Python. 

- Jekyll - ruby - https://jekyllrb.com/
- Hexo - Node - https://hexo.io/
- Hugo - Go - https://gohugo.io/
- Pelican - Python - https://blog.getpelican.com/

---
<!-- .slide: data-background-image="img/Git-merge-2018/Git-merge-2018.024.jpeg" -->

Note: 
Similarly, you don't have to build multiple sites in order to get this started. 
I'd recommend starting one of these - like have a blog where the authors write their posts in markdown. 

https://about.gitlab.com/2016/06/17/ssg-overview-gitlab-pages-part-3-examples-ci/

Here is a blog post written by my colleague Marcia (it happens to about about static site generators)

At GitLab, everyone who blogs does it in markdown 

And once you systems like this, where blogs are written in markdown, versioned in Git and published with a Static Site Generator you start to unlock some magic. 

Because one person writing a blog post in nice, but the tradeoffs are not necessarily any better or worse that you would get from blogging in a CMS. 

But what about if you had not just one person writing a post, but 20 people all trying to collaborate on writing a single blog post at the time time? That becomes pretty hard in a CMS. 

Now, you might ask, when on Earth do you need 20 different people all writing a blog post together? 

+++
<!-- .slide: data-background-image="img/Git-merge-2018/Git-merge-2018.027.jpeg" -->

Note:

Here is a blog post written by my colleague Marcia (it happens to about about static site generators)

At GitLab, everyone who blogs does it in markdown 

And once you systems like this, where blogs are written in markdown, versioned in Git and published with a Static Site Generator you start to unlock some magic. 

Because one person writing a blog post in nice, but the tradeoffs are not necessarily any better or worse that you would get from blogging in a CMS. 

But what about if you had not just one person writing a post, but 20 people all trying to collaborate on writing a single blog post at the time time? That becomes pretty hard in a CMS. 

Now, you might ask, when on Earth do you need 20 different people all writing a blog post together? 

---
<span class="menu-title" style="display: none">New Features Every Month</span>
<!-- .slide: data-background-image="img/Git-merge-2018/Git-merge-2018.029.jpeg" -->

Note:
Well, at GitLab, this is something we do every single month. Because of our development schedule we ship a new release of GitLab every single month on the 22nd. It’s like clockwork. And of course when we ship a new version we need to tell folks about the features. So GitLab ships a release post.

Let’s take a look at the latest one:  

https://about.gitlab.com/2018/02/22/gitlab-10-5-released/

This is the release post, we tend to ship ~30 features with each release. 

https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/9224/diffs

Here is the merge request the Release notes blog post for GitLab 10.5 the just shipped on Feb 22. Now this is a really involved effort. There is not way we could get this thing out if everyone in the company had to community to one author and that person had to put the post together on their own. 

This one had 29 participants 

With 138 commits

And this is not people writing code - but’ it’s mostly collaborating on blog content in markdown And YAML

And what you get this this really collaborative process - where you can comment on specific lines, people can suggest edits by adding a new commit, or they can comment on a line of copy. The same way that developers do collaborative code view, we do collaborative blog review. 

There’s really no other way to do it - if we didn’t structure things in this way, we couldn’t get this out every month. 

+++
<!-- .slide: data-background-image="img/Git-merge-2018/releasepost.png" -->

Note:
This is the release post, we tend to ship ~30 features with each release. 


+++
<!-- .slide: data-background-image="img/Git-merge-2018/Git-merge-2018.030.jpeg" -->

Note:
Here is the merge request the Release notes blog post for GitLab 10.5 the just shipped on Feb 22. Now this is a really involved effort. There is not way we could get this thing out if everyone in the company had to community to one author and that person had to put the post together on their own. 

+++
<!-- .slide: data-background-image="img/Git-merge-2018/Git-merge-2018.031.jpeg" -->

Note:
This one had 29 participants 

+++
<!-- .slide: data-background-image="img/Git-merge-2018/Git-merge-2018.032.jpeg" -->

Note:
With 138 commits

+++
<!-- .slide: data-background-image="img/Git-merge-2018/Git-merge-2018.033.jpeg" -->

Note:
And this is not people writing code - but’ it’s mostly collaborating on blog content in markdown 

+++
<!-- .slide: data-background-image="img/Git-merge-2018/Git-merge-2018.034.jpeg" -->

Note:
And YAML

+++
<!-- .slide: data-background-image="img/Git-merge-2018/Git-merge-2018.035.jpeg" -->

Note:
And what you get this this really collaborative process - where you can comment on specific lines, people can suggest edits by adding a new commit, or they can comment on a line of copy. The same way that developers do collaborative code view, we do collaborative blog review. 

There’s really no other way to do it - if we didn’t structure things in this way, we couldn’t get this out every month. 



---
<!-- .slide: data-background-image="img/Git-merge-2018/Git-merge-2018.036.jpeg" -->

Note:
If you think, that’s pretty awesome, I’d love to do that same thing, well the entire process is documented with step-by-step instructions on our publicly accessible handbook. 

https://about.gitlab.com/handbook/marketing/blog/release-posts/

This is really a place I recommend you start. Put up a blog on a SSG, and ship a release post when you ship new features. Invite your sales and marketing people to collaborate - you can say, hey we want your input, and they’ll say, “ah that’s really technical stuff I don’t I think can jump in, and you’ll say, “Oh, it’s just Markdown, you can learn it in about 5 minutes.” 

---
<span class="menu-title" style="display: none">Some way to stop explosions</span>
<!-- .slide: data-background-image="img/Git-merge-2018/roadmap.004.jpeg" -->

Note:
Next up, how do you make sure non-developers don't blow up your site? You need some safeguards. After all you wouldn't want to give commit access to a user who could put in a syntax error and your website ends up looking like this:

---
<!-- .slide: data-background-image="img/Git-merge-2018/broken-site.png" -->

Note:
So this bad because your site is down, but it's also not a great experience for a non-technical user trying to figure out what happened. So to empower them you need to automate your testing and deployment with 

---
<!-- .slide: data-background-image="img/Git-merge-2018/Git-merge-2018.040.jpeg" -->

Note:
CI/CD Pipelines. To show how powerful this is, let me tell you about a colleague of mind who just recently joined GitLab. 

https://about.gitlab.com/team/

http://get.gitlab.com/git-merge/

+++
<!-- .slide: data-background-image="img/Git-merge-2018/Git-merge-2018.041.jpeg" -->

Note:
Now when you join GitLab, one of the first things we make you do is update the website. So we say, "Welcome to GitLab, you need to know how to update the website." So we have this team page - it's public-facing company directory and the way you add yourself is be creating a merge request. 

+++
<!-- .slide: data-background-image="img/Git-merge-2018/Git-merge-2018.042.jpeg" -->

Note:
So this is my new friend Suri. She is a content marketing associate, and although she quite a talented writer, she does not have a CS or software development background. So this task of updating the website was her first time working with code. 

https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/10029/diffs

+++
<!-- .slide: data-background-image="img/Git-merge-2018/Git-merge-2018.043.jpeg" -->

Note:
So this is her merge request she created. She updated the yml file to include her details and submitted the MR. 

+++
<!-- .slide: data-background-image="img/Git-merge-2018/Git-merge-2018.044.jpeg" -->

Note:
But as it turned out the pipeline failed. So Suri got a pretty clear message that something was not quite right, but it was done in as safe environment where the error doesn't take down the site. You can't expect a non-developer to have their own local dev environment and troubleshoot errors. You need CI/CD to automate this. 

+++
<!-- .slide: data-background-image="img/Git-merge-2018/Git-merge-2018.045.jpeg" -->

Note:
So, what Suri did was compare the text she had written with what others had put on the page. This is a network effect - when you everyone following this process this type of benefit emerges. And she noticed that she had added an @ symbol to her twitter handle where as other folks did not. 

+++
<!-- .slide: data-background-image="img/Git-merge-2018/Git-merge-2018.046.jpeg" -->

Note:
Now if we dig in to her pipeline error. We can see there was a problem caught by the linter. 

+++
<!-- .slide: data-background-image="img/Git-merge-2018/Git-merge-2018.047.jpeg" -->

Note:
And checking the output specifically we can see that it was in fact the at symbol creating a syntax error. 

+++
<!-- .slide: data-background-image="img/Git-merge-2018/Git-merge-2018.048.jpeg" -->

Note:
But, what is so impressive about this, is that Suri was able to figure this out all on her own without even knowing what a syntax error is. I think part of this is because Suri is pretty sharp, and part of this is because she was put into an environment that was designed to make her successful. 

https://about.gitlab.com/team/#suripatel

---
<!-- .slide: data-background-image="img/Git-merge-2018/Git-merge-2018.040.jpeg" -->

Note:
Now, when most folks think about implementing CI/CD pipelines, they are going to go to the effort to build pipelines for application code, but they don't necessarily think about automating test and deploy for their blog. And what I am telling you is that this is a critical part of making non-developers successful. 

---
<span class="menu-title" style="display: none">The Stack: CI/CD</span>
<!-- .slide: data-background-image="img/Git-merge-2018/Git-merge-2018.011.jpeg" -->

Note:
One note about the stack - GitLab, Bitbucket, and VSTS, all have built-in CI/CD Pipelines as part of the product. With GitHub you'll have to use a 3rd party in order to get CI/CD. But the good news is that GitLab has a new feature shipping in the very next release on the 22nd of this month: 

---
### GitLab CI/CD for GitHub

Note:
So you can use GitHub as your code repository, and you can use GitLab as CI/CD.

So, to wrap things up... 

---
### Resource Slide
<small>
- https://gitlab.com/williamchia/git-merge-2018
- https://gitlab.com/help/user/markdown
- https://about.gitlab.com/2016/06/17/ssg-overview-gitlab-pages-part-3-examples-ci/
- https://about.gitlab.com/2018/02/22/gitlab-10-5-released/
- https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/9224/diffs
- https://middlemanapp.com/
- https://nanoc.ws/
- https://jekyllrb.com/
- https://hexo.io/
- https://gohugo.io/
- https://blog.getpelican.com/
- https://about.gitlab.com/handbook/marketing/blog/release-posts/
- https://gitlab.com/gitlab-com/www-gitlab-com/blob/master/.gitlab-ci.yml
- https://about.gitlab.com/team/
- http://get.gitlab.com/git-merge/

</small>

Note:
Here is the resources slide I promised. You can find this presentation and these links on gitlab.com/williamchia/git-merge-2018

So let's summarize what we covered. 

---
### Empower non-developers to use Git 
- something to version <!-- .element: class="fragment"  -->
  - Markdown <!-- .element: class="fragment"  -->
- somewhere to publish <!-- .element: class="fragment"  -->
  - Static Site Generator <!-- .element: class="fragment"  -->
- some way to stop explosions <!-- .element: class="fragment"  -->
  - CI/CD <!-- .element: class="fragment"  -->

---
Going meta

Note:
So, I'd like to leave you with a final story.

I have this problem today - collaborate and versioning slide decks. We already use google sheets, but it’s still pretty painful. Getting back to older versions, seeing who contributed what, and then getting feedback on the changes, it just doesn’t work. Especially when you want to the have the scenario of 2 people developing on the same asset concurrently and then merging the result - it just can't be done. 

So what can I do about it? 

https://gitlab.com/williamchia/git-merge-2018/blob/master/slides.md

---
Version control your Slides

Note:
The answer is to version control your slides.

There's a framework called Reveal.js that allows you to build your content in Markdown and have it converted into slides that you can present. In fact, this deck that I've been presenting to you right now including all of my slides and presenter notes was built in markdown. 

If you go to the repo I've been talking about, you can see that here is my slides.md file, and here is the markdown that I wrote. 

+++
<!-- .slide: data-background-image="img/Git-merge-2018/slides-md.png" -->

Note:
My pal Brendan O’Leary, who's in charge of our customer training program, first show showed me how to do this. He was running into very same problem. He wanted to collaborate on slides, but it was too hard with the existing technologies. By building slides in Markdown they become version-able via Git which allows you to collaborate at scale involving even more people in the process. 

I built this deck in Markdown, and my next step is build more decks using the same fundamental principles so that many people can collaborate on them continuing to make it really simple for non-developers to use Git everyday. 

---
# Thanks! 
