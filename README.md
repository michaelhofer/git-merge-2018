# GitLab @ GitMere 2018

This is a presentation from GitMerge 2018 https://git-merge.com/

# Resources
- https://gitlab.com/williamchia/git-merge-2018
- https://about.gitlab.com/2016/06/17/ssg-overview-gitlab-pages-part-3-examples-ci/
- https://middlemanapp.com/
- https://nanoc.ws/
- https://jekyllrb.com/
- https://hexo.io/
- https://gohugo.io/
- https://blog.getpelican.com/
- https://about.gitlab.com/handbook/marketing/blog/release-posts/
- https://gitlab.com/gitlab-com/www-gitlab-com/blob/master/.gitlab-ci.yml
- https://about.gitlab.com/team/
- http://get.gitlab.com/git-merge/

# Getting Started

Build your own slides with Reveal.js:

To get started:

1.  Clone or download this repo and make your own copy of it
1.  Add slides to the slides.md
1.  Serve the repo in a simple web server
    - For instance, in the root of your repo run `python -m SimpleHTTPServer 8000`
    - Navigate to [http://localhost:8000](http://localhost:8000)
1. Enable pages
1.  ?
1.  Profit!

In addition, once you push to GitLab, your presentation will automatically be available via GitLab pages!